resource "aws_lambda_function" "this" {
  function_name = "api"
  role          = aws_iam_role.lambda.arn

  filename         = var.lambda_fn_payload
  source_code_hash = filebase64sha256(var.lambda_fn_payload)

  handler = var.handler
  runtime = var.runtime

  vpc_config {
    subnet_ids         = [aws_subnet.api.id]
    security_group_ids = [aws_security_group.api.id]
  }

  memory_size = var.memory_size
  timeout     = var.timeout
}

resource "aws_iam_role" "this" {
  name               = "${aws_lambda_function.this.function_name}-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_role.json
}

data "aws_iam_policy_document" "lambda_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"
      identifiers = [
        "lambda.amazonaws.com",
      ]
    }
  }
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
