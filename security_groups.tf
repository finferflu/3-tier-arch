# Global security group rules to allow egress to the world
resource "aws_security_group_rule" "egress_all" {
  for_each = local.sgs

  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = each.key
}

resource "aws_security_group_rule" "egress_all_ipv6" {
  for_each = local.sgs

  type             = "egress"
  from_port        = 0
  to_port          = 0
  protocol         = "-1"
  ipv6_cidr_blocks = ["::/0"]

  security_group_id = each.key
}

locals {
  sgs = toset([aws_security_group.db.id, aws_security_group.api.id])
}

# SG for the database
resource "aws_security_group" "db" {
  name        = "db-sg"
  description = "DB Security Group"
  vpc_id      = aws_vpc.this.id
}

# SG for the API
resource "aws_security_group" "api" {
  name        = "api-sg"
  description = "API Security Group"
  vpc_id      = aws_vpc.this.id
}

# Grant access between the DB and API SGs to port 3306
resource "aws_security_group_rule" "allow_rds_access" {
  type      = "ingress"
  from_port = 3306
  to_port   = 3306
  protocol  = "tcp"

  security_group_id        = aws_security_group.db.id
  source_security_group_id = aws_security_group.api.id
}


