resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
}

resource "aws_internet_gateway" "this" {
  vpc_id = aws_vpc.this.id
}

# Default route providing access to the world
resource "aws_route" "this" {
  route_table_id         = aws_vpc.this.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.this.id
}

# NAT GW
resource "aws_nat_gateway" "this" {
  allocation_id = aws_eip.this.id
  subnet_id     = aws_subnet.access.id
}

resource "aws_eip" "this" {
  vpc = true
}

# Public subnet for the NAT GW (uses the default route)
resource "aws_subnet" "access" {
  vpc_id            = aws_vpc.this.id
  cidr_block        = var.subnet_access_cidr
  availability_zone = "${var.aws_region}a"
}

# Route table for private networks, routing through the NAT GW
resource "aws_route_table" "this" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.this.id
  }
}

# Private subnet used by the DB 
resource "aws_subnet" "db" {
  count = 2

  vpc_id            = aws_vpc.this.id
  cidr_block        = var.subnet_db_cidr
  availability_zone = "${var.aws_region}a"
}

# Private subnet used by the API
resource "aws_subnet" "api" {
  vpc_id            = aws_vpc.this.id
  cidr_block        = var.subnet_api_cidr
  availability_zone = "${var.aws_region}a"
}

# Route private subnets via NAT GW
resource "aws_route_table_association" "db" {
  for_each = aws_subnet.db

  subnet_id      = each.value.id
  route_table_id = aws_route_table.this.id
}

resource "aws_route_table_association" "api" {
  subnet_id      = aws_subnet.api.id
  route_table_id = aws_route_table.this.id
}
