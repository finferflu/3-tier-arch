# RDS cluster for the data layer
resource "aws_rds_cluster" "this" {
  engine                 = "aurora-mysql"
  cluster_identifier     = "db-cluster"
  database_name          = var.db_name
  master_username        = var.master_username
  master_password        = var.master_password
  vpc_security_group_ids = aws_security_group.db.id
  db_subnet_group_name   = aws_db_subnet_group.this.id
}

resource "aws_db_subnet_group" "this" {
  name       = "db-subnet-group"
  subnet_ids = [for s in aws_subnet.db : s.id]
}

resource "aws_rds_cluster_instance" "this" {
  identifier           = "db-instance"
  cluster_identifier   = aws_rds_cluster.this.id
  instance_class       = var.instance_class
  db_subnet_group_name = aws_db_subnet_group.this.id
}
