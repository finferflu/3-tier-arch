variable "aws_region" {
  description = "The AWS region. Used by Terraform to create resources."
}

variable "aws_access_key" {
  description = "The AWS access key ID. Used by Terraform to create resources."
}

variable "aws_secret_key" {
  description = "The AWS secret access key. Used by Terraform to create resources."
}

variable "lambda_fn_payload" {
  description = "A zip archive."
}

variable "handler" {
  description = "The Lambda function handler."
}

variable "runtime" {
  description = "The Lambda function runtime."
}

variable "memory_size" {
  description = "The Lambda function memory size."
}

variable "timeout" {
  description = "The Lambda function timeout."
}

variable "db_name" {
  description = "The DB name."
}

variable "master_username" {
  description = "The DB master username."
}

variable "master_password" {
  description = "The DB master password."
}

variable "instance_class" {
  description = "The DB instance class."
}

variable "vpc_cidr" {
  description = "The VPC CIDR block."
}

variable "subnet_access_cidr" {
  description = "The CIDR for the access subnet."
}

variable "subnet_db_cidr" {
  description = "The CIDR for the DB subnet."
}

variable "subnet_api_cidr" {
  description = "The CIDR for the API subnet."
}
