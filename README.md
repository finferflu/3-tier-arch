# 3-tier-arch

A Terraform-based sample 3-tier architecture, intended as a serverless back-end and a Jamstack front-end, featuring:

- **Business Tier**: API Gateway + Lambda
- **Data Tier**: RDS
- **Presentation Tier**: CloudFront + S3 bucket

![Diagram](diagram.png)
