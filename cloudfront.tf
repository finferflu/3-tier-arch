# CloudFront distribution for the presentation layer
resource "aws_cloudfront_distribution" "this" {
  enabled = true

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  origin {
    domain_name = aws_s3_bucket.this.website_endpoint
    origin_id   = aws_s3_bucket.this.name
  }


  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.this.name

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}

# Public S3 bucket for the front-end app
resource "aws_s3_bucket" "this" {
  bucket = "front-end"
  acl    = "public-read"
  policy = data.aws_iam_policy_document.public_access.json

  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

# Grant public access to the S3 bucket
data "aws_iam_policy_document" "public_access" {
  statement {
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.this.name}/*",
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}
